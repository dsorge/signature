package de.dsorge.signature.ui;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JavaScript;

/**
 * 
 * @author dsorge
 *
 */
@Tag("canvas")
@JavaScript("https://cdn.jsdelivr.net/npm/signature_pad@3.0.0-beta.4/dist/signature_pad.umd.min.js")
public class SignatureField extends Component implements HasStyle {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6803595873270162971L;

	public SignatureField() {
		setClassName("signatureField");
	}

	@Override
	protected void onAttach(final AttachEvent attachEvent) {
		super.onAttach(attachEvent);
		getUI().ifPresent(ui -> ui.getPage().executeJs("var canvas = document.querySelector(\"canvas\");\n" + "\n"
				+ "window.signaturePad = new SignaturePad(canvas);"));
	}

	public void clearContent() {
		getUI().ifPresent(ui -> ui.getPage().executeJs("window.signaturePad.clear();"));
	}

	public void saveAsPng() {
		getUI().ifPresent(ui -> ui.getPage().executeJs("window.signaturePad.toDataURL();"));
	}
}
