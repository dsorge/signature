package de.dsorge.signature.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

/**
 * A sample Vaadin view class.
 * <p>
 * To implement a Vaadin view just extend any Vaadin component and use @Route
 * annotation to announce it in a URL as a Spring managed bean.
 * <p>
 * A new instance of this class is created for every new user and every browser
 * tab/window.
 */
@Route
@CssImport("./styles/shared-styles.css")
public class MainView extends VerticalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4306078171791503916L;

	/**
	 * Construct a new Vaadin view.
	 * <p>
	 * Build the initial UI state for the user accessing the application.
	 *
	 */
	public MainView() {

		// Use custom CSS classes to apply styling. This is defined in
		// shared-styles.css.
		addClassName("centered-content");

		final SignatureField signatureField = new SignatureField();
		add(signatureField);

		final Button clearButton = new Button("Clear", e -> signatureField.clearContent());
		add(clearButton);

		final Button saveButton = new Button("Save", e -> signatureField.saveAsPng());
		add(saveButton);
	}

}
